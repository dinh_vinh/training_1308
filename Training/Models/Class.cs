﻿using System.ComponentModel;

namespace Training.Models
{
    public class Class
    {
        public int Id { get; set; }
        [DisplayName("Mã Lớp :")]
        public  string MaLop { get; set; }
        [DisplayName("Tên Lớp :")]
        public string TenLop { get; set; }
    }
}