﻿using System.ComponentModel;

namespace Training.Models
{
    public  class Student
    {
      
        public int Id { get; set; }
        [DisplayName("Mã sinh viên :")]
        public string MaSV { get; set; }
        [DisplayName("Họ Tên :")]
        public string HoTen { get; set; }
    }
}