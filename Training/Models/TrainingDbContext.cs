﻿using Microsoft.EntityFrameworkCore;

namespace Training.Models
{
    public class TrainingDbContext : DbContext
    {
        public TrainingDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Student> Student { get; set; }
        public DbSet<Class> Class { get; set; }

    }
}