﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Training.Models;
using Training.Repository;
using Training.UnitOfWork;

namespace Training.Service
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;
        public StudentService(IUnitOfWork  unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Student GetById(int? Id)
        {
            return _unitOfWork.studentRepository.GetById(Id);
        }

        public IEnumerable<Student> GetAll()
        {
            return _unitOfWork.studentRepository.GetAll();
        }

        public Task Create(Student student)
        {
            return _unitOfWork.studentRepository.Create(student);
        }

        public void Delete(int Id)
        {
            _unitOfWork.studentRepository.Delete(Id);
        }

        public Task Update(Student student)
        {
            return _unitOfWork.studentRepository.Update(student);
        }

        public void save()
        {
            _unitOfWork.studentRepository.save();
        }
    }
}