﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Training.Models;
using Training.Repository;

namespace Training.Service
{
    
    public interface IStudentService
    {
        Student GetById(int? Id);
        IEnumerable<Student> GetAll();
        Task Create(Student  student );
        void Delete(int Id);
        Task Update(Student student);
        void save();
    }
}