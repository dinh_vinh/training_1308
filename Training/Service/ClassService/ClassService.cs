﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Training.Models;
using Training.Repository;
using Training.UnitOfWork;

namespace Training.Service
{
    public class ClassService : IClassService
    {

        private readonly IUnitOfWork _unitOfWork;
        public ClassService(IUnitOfWork  unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Class GetById(int? Id)
        {
            return _unitOfWork.classRepository.GetById(Id);
        }

        public IEnumerable<Class> GetAll()
        {
            return  _unitOfWork.classRepository.GetAll();
        }

        public Task Create(Class classS)
        {
           return  _unitOfWork.classRepository.Create(classS);
        }

        public void Delete(int Id)
        {
            _unitOfWork.classRepository.Delete(Id);
        }

        public Task Update(Class classS)
        {
            return  _unitOfWork.classRepository.Update(classS);
        }

        public void save()
        {
            _unitOfWork.classRepository.save();
        }
    }

}