﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Training.Models;
using Training.Repository;

namespace Training.Service
{
    public interface  IClassService
    {
        Class GetById(int? Id);
        IEnumerable<Class> GetAll();
        Task Create(Class  classS );
        void Delete(int Id);
        Task Update(Class classS);
        void save();

    }
}