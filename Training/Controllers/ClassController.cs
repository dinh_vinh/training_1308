﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Training.Models;
using Training.Service;

namespace Training.Controllers
{
    public class ClassController : Controller
    {
        private readonly IClassService _classService;

        public ClassController (IClassService classService)
        {
            _classService = classService;
        }
       
        [HttpGet]
        public IActionResult Index()
        {
            return View(_classService.GetAll());
        }
       
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Create(Class class1)
        {
            
            if(ModelState.IsValid)
            {
                _classService.Create(class1);
                _classService.save();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create");
        }

        [HttpGet]
        public IActionResult Delete(int? Id)
        {
            return View(_classService.GetById(Id));
        }
        public IActionResult Delete(int Id)
        {
            _classService.Delete(Id);
            _classService.save();
            return RedirectToAction("Index");

        }
        [HttpGet]
        public IActionResult Edit(int? Id)
        { 
            return View(_classService.GetById(Id));
        }
        public async Task<IActionResult> Edit(Class class1)
        { 
            _classService.Update(class1);
            _classService.save();
            return RedirectToAction("Index");

        }
        
        [HttpGet]
        public IActionResult GetId()
        {
            return View();
        }
        
    }
}