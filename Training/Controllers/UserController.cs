﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Training.Models;
using Training.Service;
using Training.UnitOfWork;

namespace Training.Controllers
{
    public class UserController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserController(IUnitOfWork  unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
//         GET
        [HttpGet]
        public IActionResult Create()
        {

            return View();

        }

        [HttpPost]
        public async Task<IActionResult> Create(Class class1)
        {
            var student = new Student()
            {
                MaSV = Request.Form["MaSV"],
                HoTen = Request.Form["HoTen"],
            };
            var classs = new Class()
            {
                MaLop = Request.Form["MaLop"],
                TenLop = Request.Form["TenLop"]
            };
        if(ModelState.IsValid)
            {
               _unitOfWork.classRepository.Create(classs);
               _unitOfWork.studentRepository.Create(student);
               _unitOfWork.save();
              
                return RedirectToAction("Create");
            }
            return RedirectToAction("Create");
        }

       

        public IActionResult Edit()
        {
            throw new System.NotImplementedException();
        }
    }

    internal class ClasstService
    {
    }
}