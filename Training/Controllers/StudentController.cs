﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Training.Models;
using Training.Service;

namespace Training.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }
       
        [HttpGet]
        public IActionResult Index()
        {
            return View(_studentService.GetAll());
        }
       
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Create( Student student)
        {
           
            if(ModelState.IsValid)
            {
                _studentService.Create(student);
                _studentService.save();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create");
        }

        [HttpGet]
        public IActionResult Delete(int? Id)
        {
            return View(_studentService.GetById(Id));
        }
        public IActionResult Delete(int Id)
        {
            _studentService.Delete(Id);
            _studentService.save();
            return RedirectToAction("Index");

        }
        [HttpGet]
        public IActionResult Edit(int? Id)
        { 
            return View(_studentService.GetById(Id));
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Student student)
        { 
            _studentService.Update(student);
            _studentService.save();
            return RedirectToAction("Index");

        }

    }
}