﻿using Training.Models;
using Training.Repository;
using Training.Service;

namespace Training.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private TrainingDbContext _context;

        public UnitOfWork(TrainingDbContext context)
        {
            _context = context;
            IniRepositories();
        }


        public IGenericRepository<Class> classRepository { get; private set; }
        public IGenericRepository<Student> studentRepository { get; private set; }

        private void IniRepositories()
        {
            studentRepository = new GenericRepository<Student>(_context);
            classRepository = new GenericRepository<Class>(_context);
          
        }

      
        public void save()
        {
            _context.SaveChanges();
        }
    }
    
}