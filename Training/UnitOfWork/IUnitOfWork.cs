﻿using Training.Models;
using Training.Repository;
using Training.Service;

namespace Training.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Class> classRepository { get; }
        IGenericRepository<Student> studentRepository { get; }
        void save();

    }
}