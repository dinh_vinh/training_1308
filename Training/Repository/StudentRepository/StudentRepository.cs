﻿using Training.Models;

namespace Training.Repository
{
    public class StudentRepository : GenericRepository<Student>,IStudentRepository
    {
        public StudentRepository(TrainingDbContext context) : base(context)
        {
        }
       
    }
}