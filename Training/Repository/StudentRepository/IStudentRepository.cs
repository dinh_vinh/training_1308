﻿using Training.Models;

namespace Training.Repository
{
    public interface IStudentRepository : IGenericRepository<Student>
    {
       
    }
        
}