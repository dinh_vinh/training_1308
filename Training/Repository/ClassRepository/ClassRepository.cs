﻿using Training.Models;

namespace Training.Repository
{
    public class ClassRepository : GenericRepository<Class>, IClassRepository
    {
        public ClassRepository(TrainingDbContext context) : base(context)
        {
        }
    }
}