﻿using Training.Models;
using Training.Service;

namespace Training.Repository
{
    public interface IClassRepository : IGenericRepository<Class>
    {
        
    }
}